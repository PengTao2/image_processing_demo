#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,gray;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	GaussianBlur(src,dst,Size(3,3),0,0);
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	cvtColor(dst,gray,CV_BGR2GRAY);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);

	Mat xdst,ydst;
// 	Scharr(gray,xdst,CV_16S,1,0);
// 	Scharr(gray,ydst,CV_16S,0,1);
	Sobel(gray,xdst,CV_16S,1,0,3);
	Sobel(gray,ydst,CV_16S,0,1,3);
	convertScaleAbs(xdst,xdst);
 	convertScaleAbs(ydst,ydst);

// 	namedWindow("xdst",CV_WINDOW_AUTOSIZE);
// 	imshow("xdst",xdst);
// 	namedWindow("ydst",CV_WINDOW_AUTOSIZE);
// 	imshow("ydst",ydst);

	Mat xydst = Mat(xdst.size(),xdst.type());
	int cols = xdst.cols;
	int rows = xdst.rows;
	for (int row = 0; row < rows;row++)
	{
		for (int col = 0;col < cols;col++)
		{
			int xv = xdst.at<uchar>(row,col);
			int yv = ydst.at<uchar>(row,col);
			xydst.at<uchar>(row,col) = saturate_cast<uchar>(xv+yv);
		}
	}
	//addWeighted(xdst,0.5,ydst,0.5,0,xydst);
	namedWindow("xydst",CV_WINDOW_AUTOSIZE);
	imshow("xydst",xydst);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}

