#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,gray,bin,dst;
int m_threshold = 100;
void CallBack(int,void*);
int main(int argc,int argv)
{
	src = imread("D:/OpenCVTest/Images/tempsrc.jpg");
	if(src.empty())
	{
		cout<<"Could not load image..."<<endl;
		return -1;
	}
	namedWindow("gray");
	namedWindow("bin");
	namedWindow("src");
	namedWindow("dst");
	imshow("src",src);

	cvtColor(src,gray,CV_BGR2GRAY);
	imshow("gray",gray);

	createTrackbar("threshold:","src",&m_threshold,255,CallBack);
	CallBack(0,0);

	waitKey(0);
	return 0;
}
void CallBack(int,void*)
{
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	threshold(gray,bin,m_threshold,255,THRESH_BINARY);
	imshow("bin",bin);
	findContours(bin,contours,hierarchy,RETR_TREE,CHAIN_APPROX_SIMPLE);

	vector<vector<Point>> polys(contours.size());
	vector<Rect> rects(contours.size());
	vector<Point2f> ccs(contours.size());
	vector<float> radius(contours.size());

	for(int i = 0;i < contours.size();i++)
	{
		/*������������*/
		approxPolyDP(contours[i],polys[i],3,true);
		/*Ѱ����С����*/
		rects[i] = boundingRect(polys[i]);
		/*Ѱ����СԲ*/
		minEnclosingCircle(polys[i],ccs[i],radius[i]);
	}
	src.copyTo(dst);
	for (int i = 0;i < contours.size();i++)
	{
		rectangle(dst,rects[i],Scalar(0,0,255),1,LINE_AA);
		circle(dst,ccs[i],radius[i],Scalar(0,255,0),1,LINE_AA);
	}
	imshow("dst",dst);
	return;
}

