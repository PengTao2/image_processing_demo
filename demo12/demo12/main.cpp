#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;
Mat src,dst;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/4.jpg");
	if(src.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	Mat kernel = getStructuringElement(MORPH_RECT,Size(11,11),Point(-1,-1));
	morphologyEx(src,dst,CV_MOP_BLACKHAT,kernel);


	imshow("dst",dst);
	waitKey(0);
	return 0;
}
