#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

string double2String(double d);
void CallBack(int,void*);

Mat src,temp,res,dst;
int method = 0;
//计算hue直方图
Mat hue_hist;
//申明直方图H通道等级数，与S通道等级数
int h_bin = 10;
//申明直方图H通道阈值范围，与S通道阈值范围
float h_range[] = {0,180};
const float* ranges[] = {h_range};
//申明图像通道数H,S两个通道
int channels = 0;
//申明直方图输出结果因为是计算多通道图像的直方图结果所以用MatND申明

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/tempsrc.jpg");
	temp = imread("D:/OpenCVTest/Images/temp.jpg");
	if(src.empty() || temp.empty())
	{
		cout<<"Could not load image"<<endl;
		return -1;
	}
	namedWindow("src");
	namedWindow("temp");
	namedWindow("dst");
	imshow("src",src);
	imshow("temp",temp);

	int w = src.cols - temp.cols + 1;
	int h = src.rows - temp.rows + 1;
	res = Mat(Size(h,w),CV_32FC1);

	createTrackbar("method:","src",&method,5,CallBack);
	CallBack(0,0);

	waitKey(0);
	return 0;
}
void CallBack(int,void*)
{
	matchTemplate(src,temp,res,method);
	normalize(res,res,0,1,NORM_MINMAX);
	Point minloc,maxloc,temploc;
	double min,max;
	minMaxLoc(res,&min,&max,&minloc,&maxloc);
	if(method == CV_TM_SQDIFF || method == CV_TM_SQDIFF_NORMED)
	{
		temploc = minloc;
	}
	else 
	{
		temploc = maxloc;
	}
	src.copyTo(dst);
//	rectangle(dst,Rect(minloc.x,minloc.y,temp.cols,temp.rows),Scalar(255,0,0),2,LINE_AA);
//	rectangle(dst,Rect(maxloc.x,maxloc.y,temp.cols,temp.rows),Scalar(0,255,0),2,LINE_AA);
//	rectangle(dst,Rect(temploc.x,temploc.y,temp.cols,temp.rows),Scalar(0,255,255),2,LINE_AA);
	rectangle(dst,Rect(temploc.x,temploc.y,temp.cols,temp.rows),Scalar::all(255),2,LINE_AA);
	imshow("dst",dst);
}

