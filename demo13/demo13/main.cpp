#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

Mat src,gray,bin,dst;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/chars.png");
	if(src.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	cvtColor(src,gray,CV_RGB2GRAY);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);

	adaptiveThreshold(~gray,bin,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,15,-2);
	namedWindow("bin",CV_WINDOW_AUTOSIZE);
	imshow("bin",bin);

	Mat kernel = getStructuringElement(MORPH_RECT,Size(3,3));
	Mat hline = getStructuringElement(MORPH_RECT,Size(bin.cols / 16,1),Point(-1,-1));
	//Mat hline = getStructuringElement(MORPH_RECT,Size(1,1),Point(-1,-1));
	//Mat vline = getStructuringElement(MORPH_RECT,Size(1,bin.rows / 16),Point(-1,-1));
	morphologyEx(bin,dst,CV_MOP_OPEN,kernel);

	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	waitKey(0);
	return 0;
}
