#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,dst1;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);
	
	//int top = 0.05 * src.rows;
	//int bottom = 0.05 * src.rows;
	//int left= 0.05 * src.cols;
	//int right = 0.05 * src.cols;
// 	int top = 20;
// 	int bottom = 20;
// 	int left= 20;
// 	int right = 20;
// 	RNG rng(12345);
// 	int borderType = BORDER_REPLICATE;
// 	while(true)
// 	{
// 		waitKey(500);
// 		Scalar color = Scalar(rng.uniform(0,255),rng.uniform(0,255),rng.uniform(0,255));
// 		copyMakeBorder(src,dst,top,bottom,left,right,borderType,color);
// 		imshow("dst",dst);
// 	}
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	GaussianBlur(src,dst,Size(3,3),0,0,BORDER_DEFAULT);
	imshow("dst",dst);

	namedWindow("dst1",CV_WINDOW_AUTOSIZE);
	GaussianBlur(src,dst1,Size(3,3),55,55,BORDER_DEFAULT);
	imshow("dst1",dst1);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}

