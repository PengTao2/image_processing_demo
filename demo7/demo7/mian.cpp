#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc,char** argv)
{
	Mat src,dst;
	src = imread("D:/OpenCVTest/Images/4.jpg");
	if(src.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	cvtColor(src,src,CV_BGR2GRAY);
	imshow("src",src);

	int rows = src.rows;
	int cols = src.cols;
	dst.create(src.size(),src.type());
	float alpth = 1.2;
	float beta = 30;
	for (int row = 0;row < rows;row ++)
	{
		for (int col = 0;col < cols;col++)
		{
			if(src.channels() == 1)
			{
				int v = src.at<uchar>(row,col);
				dst.at<uchar>(row,col) = saturate_cast<uchar>(alpth * v + beta);
			}
			else if(src.channels() == 3)
			{
				int b = src.at<Vec3b>(row,col)[0];
				int g = src.at<Vec3b>(row,col)[1];
				int r = src.at<Vec3b>(row,col)[2];

				dst.at<Vec3b>(row,col)[0] = saturate_cast<uchar>(alpth * b + beta);
				dst.at<Vec3b>(row,col)[1] = saturate_cast<uchar>(alpth * g + beta);
				dst.at<Vec3b>(row,col)[2] = saturate_cast<uchar>(alpth * r + beta);
			}
		}
	}
	




	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	waitKey(0);
	return 0;
}

