#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dstx,dsty;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);


// 	Mat kernelx = (Mat_<int>(3,3) << -1,0,1,-2,0,2,-1,0,1);//Sobel算子 X方向
// 	filter2D(src,dstx,-1,kernelx);
// 	namedWindow("dstx",CV_WINDOW_AUTOSIZE);
// 	imshow("dstx",dstx);

// 	Mat kernely = (Mat_<int>(3,3) << -1,-2,-1,0,0,0,1,2,1);//Sobel算子 Y方向
// 	filter2D(src,dsty,-1,kernely);
// 	namedWindow("dsty",CV_WINDOW_AUTOSIZE);
// 	imshow("dsty",dsty);


// 	Mat kernely = (Mat_<int>(3,3) << 0,-1,0,-1,4,-1,0,-1,0);//拉普拉斯算子 Y方向
// 	filter2D(src,dsty,-1,kernely);
// 	namedWindow("dsty",CV_WINDOW_AUTOSIZE);
// 	imshow("dsty",dsty);

	namedWindow("dsty",CV_WINDOW_AUTOSIZE);
	int c = 0;
	int index = 0;
	int ksize = 5;
	while(true)
	{
		c = waitKey(500);
		if((char)c == 27)//ESC
		{
			break;
		}
		ksize = 4 + (index % 5) * 2 + 1;
		Mat kernel = Mat::ones(Size(ksize,ksize),CV_32F) / (float)(ksize * ksize);
// 		Mat kernely = (Mat_<int>(3,3) << 0,-1,0,-1,4,-1,0,-1,0);//拉普拉斯算子 Y方向
		filter2D(src,dsty,-1,kernel);
		index++;
		imshow("dsty",dsty);
	}
	

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}

