#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,gray;
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/lines.png");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}

	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	Canny(src,gray,100,500);
	cvtColor(gray,dst,CV_GRAY2BGR);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);

	vector<Vec4f> plines;
	HoughLinesP(gray,plines,1,CV_PI / 180.0,10,0,100);

	Scalar color = Scalar(0,0,255);
	for(size_t i = 0;i < plines.size();i++)
	{
		Vec4f hline = plines[i];
		Point p1 = Point(hline[0],hline[1]);
		Point p2 = Point(hline[2],hline[3]);
		line(dst,p1,p2,color,2,LINE_AA);
	}
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}
