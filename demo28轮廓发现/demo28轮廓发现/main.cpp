#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

string double2String(double d);
void CallBack(int,void*);

Mat src,gray,edge,dst;
int method = 0;
int m_threshold = 100;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/8.jpg");
	if(src.empty())
	{
		cout<<"Could not load image"<<endl;
		return -1;
	}
	namedWindow("src");
	namedWindow("gray");
	namedWindow("dst");
	imshow("src",src);

	cvtColor(src,gray,CV_BGR2BGRA);
	imshow("gray",gray);

	createTrackbar("threshold:","dst",&m_threshold,255,CallBack);
	CallBack(0,0);

	waitKey(0);
	return 0;
}
void CallBack(int,void*)
{
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	Canny(gray,edge,m_threshold,m_threshold * 2,3,false);
	findContours(edge,contours,hierarchy,RETR_TREE,CHAIN_APPROX_SIMPLE);

	dst = Mat::zeros(src.size(),CV_8UC3);
	RNG rng(1234);
	for (int i = 0;i < contours.size();i ++)
	{
		Scalar color = Scalar(rng.uniform(0,255),rng.uniform(0,255),rng.uniform(0,255));
		drawContours(dst,contours,i,color,1,LINE_8,hierarchy,0);
	}

	imshow("dst",dst);
}

