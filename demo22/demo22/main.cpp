#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,gray;
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/circle.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);
	//��ֵ�˲�
	Mat medsrc;
	medianBlur(src,medsrc,5);
	namedWindow("medsrc",CV_WINDOW_AUTOSIZE);
	imshow("medsrc",medsrc);
	//ת�Ҷ�ͼ
	cvtColor(medsrc,gray,CV_BGR2GRAY);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);
	//����Բ���
	vector<Vec3f> pcircles;
	HoughCircles(gray,pcircles,CV_HOUGH_GRADIENT,1,30,100,50,125,200);
	//ͼ�������
	src.copyTo(dst);
	for(int i = 0;i < pcircles.size();i ++)
	{
		Vec3f cc = pcircles[i];
		Point center = Point(cc[0],cc[1]);
		circle(dst,center,cc[2],Scalar(0,0,255),2,LINE_AA);
		circle(dst,center,2,Scalar(0,255,123),2,LINE_AA);
	}
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

// 	Canny(src,gray,100,500);
// 	cvtColor(gray,dst,CV_GRAY2BGR);
// 	namedWindow("gray",CV_WINDOW_AUTOSIZE);
// 	imshow("gray",gray);
// 
// 	vector<Vec4f> plines;
// 	HoughLinesP(gray,plines,1,CV_PI / 180.0,10,0,100);
// 
// 	Scalar color = Scalar(0,0,255);
// 	for(size_t i = 0;i < plines.size();i++)
// 	{
// 		Vec4f hline = plines[i];
// 		Point p1 = Point(hline[0],hline[1]);
// 		Point p2 = Point(hline[2],hline[3]);
// 		line(dst,p1,p2,color,2,LINE_AA);
// 	}
// 	namedWindow("gray",CV_WINDOW_AUTOSIZE);
// 	imshow("dst",dst);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}
