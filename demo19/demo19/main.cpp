#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,gray;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	GaussianBlur(src,dst,Size(3,3),0,0);
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	cvtColor(dst,gray,CV_BGR2GRAY);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);

	Mat lapdst;
	Laplacian(gray,lapdst,CV_16S,3);
	convertScaleAbs(lapdst,lapdst);
	threshold(lapdst,lapdst,0,255,THRESH_OTSU | THRESH_BINARY);

	namedWindow("lapdst",CV_WINDOW_AUTOSIZE);
	imshow("lapdst",lapdst);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}

