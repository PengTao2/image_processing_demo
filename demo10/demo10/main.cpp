#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc,char** argv)
{
	Mat src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);


	Mat dst;
	dst.create(src.size(),src.type());
	//medianBlur(src,dst,5);
	bilateralFilter(src,dst,15,100,3);
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	Mat finaldst;
	Mat kernel = (Mat_<char>(3,3) << 0,-1,0,-1,5,-1,0,-1,0);
	filter2D(dst,finaldst,-1,kernel,Point(-1,-1),0);

	namedWindow("finaldst",CV_WINDOW_AUTOSIZE);
	imshow("finaldst",finaldst);

	waitKey(0);
	return 0;
}

