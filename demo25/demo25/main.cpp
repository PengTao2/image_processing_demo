#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src;
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/4.jpg");
	if(src.empty())
	{
		cout<<"Could not load image"<<endl;
		return -1;
	}
	namedWindow("src");
	imshow("src",src);

	//多通道转单通道
	vector<Mat> bgr_planes;
	split(src,bgr_planes);
	//计算直方图
	Mat b_his,g_his,r_his;
	const int histSize = 256;
	float histRange[] = {0,256};
	const float *ranges = {histRange};
	const int channels = 0;
	calcHist( 
		&bgr_planes[0],//const Mat* images, 
		1,//int nimages,
		&channels,//const int* channels, 
		Mat(),//InputArray mask,
		b_his,//OutputArray hist, 
		1,//int dims, 
		&histSize,//const int* histSize,
		&ranges,//const float** ranges, 
		true,//bool uniform = true, 
		false//bool accumulate = false 
		);
	calcHist( 
		&bgr_planes[1],//const Mat* images, 
		1,//int nimages,
		&channels,//const int* channels, 
		Mat(),//InputArray mask,
		g_his,//OutputArray hist, 
		1,//int dims, 
		&histSize,//const int* histSize,
		&ranges,//const float** ranges, 
		true,//bool uniform = true, 
		false//bool accumulate = false 
		);
	calcHist( 
		&bgr_planes[2],//const Mat* images, 
		1,//int nimages,
		&channels,//const int* channels, 
		Mat(),//InputArray mask,
		r_his,//OutputArray hist, 
		1,//int dims, 
		&histSize,//const int* histSize,
		&ranges,//const float** ranges, 
		true,//bool uniform = true, 
		false//bool accumulate = false 
		);

	int hist_h = 400;
	int hist_w = 512;
	float bitch = hist_w / histSize;
	//归一化处理将各像素点的出现频次从0--几千几万，归一化到0--400，应为直方图图像的高度只有400个像素
	normalize(b_his,b_his,0,hist_h,NORM_MINMAX);
	normalize(g_his,g_his,0,hist_h,NORM_MINMAX);
	normalize(r_his,r_his,0,hist_h,NORM_MINMAX);
	//显示直方图数据
	Mat dst_hist(hist_h,hist_w,CV_8UC3,Scalar(0,0,0));
	for (int i = 1;i < histSize;i++)
	{
		Point p1 = Point((i - 1)* bitch,hist_h - b_his.at<float>(i - 1));
		Point p2 = Point((i)* bitch,hist_h - b_his.at<float>(i));
		line(dst_hist,p1,p2,Scalar(255,0,0),1,LINE_AA);

		Point p3 = Point((i - 1)* bitch,hist_h - g_his.at<float>(i - 1));
		Point p4 = Point((i)* bitch,hist_h - g_his.at<float>(i));
		line(dst_hist,p3,p4,Scalar(0,255,0),1,LINE_AA);

		Point p5 = Point((i - 1)* bitch,hist_h - r_his.at<float>(i - 1));
		Point p6 = Point((i)* bitch,hist_h - r_his.at<float>(i));
		line(dst_hist,p5,p6,Scalar(0,0,255),1,LINE_AA);
	}

	namedWindow("dst");
	imshow("dst",dst_hist);

	waitKey(0);
	return 0;
}

