#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,gray;
Mat edgedst;
int t1value = 50,t2value = 150;
void CallBack(int,void*);
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}

	resize(src,src,Size(src.cols / 3,src.rows / 3));

	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	GaussianBlur(src,dst,Size(3,3),0,0);
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	cvtColor(dst,gray,CV_BGR2GRAY);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);
	
	namedWindow("edgedst",CV_WINDOW_AUTOSIZE);
	createTrackbar("t1value:","edgedst",&t1value,255,CallBack);
	createTrackbar("t2value:","edgedst",&t2value,255,CallBack);
	CallBack(0,0);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}
void CallBack(int,void*)
{
	Mat im;
	Canny(gray,edgedst,t1value,t2value,3);

	im.create(src.size(),src.type());
	src.copyTo(im,edgedst);

	imshow("edgedst",im);
}
