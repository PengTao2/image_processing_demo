#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

string double2String(double d);
void CallBack(int,void*);

Mat src,hue;
//计算hue直方图
Mat hue_hist;
//申明直方图H通道等级数，与S通道等级数
int h_bin = 10;
//申明直方图H通道阈值范围，与S通道阈值范围
float h_range[] = {0,180};
const float* ranges[] = {h_range};
//申明图像通道数H,S两个通道
int channels = 0;
//申明直方图输出结果因为是计算多通道图像的直方图结果所以用MatND申明
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not load image"<<endl;
		return -1;
	}
	namedWindow("backPrjImage");
	namedWindow("src");
	imshow("src",src);

	cvtColor(src,src,CV_RGB2HSV);
	hue.create(src.size(),src.type());
	int nchannels[] = {0,0};
	//将src第一个 通道的数据值，放到hue
	mixChannels(&src,1,&hue,1,nchannels,1);
	
	createTrackbar("h_bin:","src",&h_bin,255,CallBack);
	CallBack(0,0);	

	waitKey(0);
	return 0;
}
void CallBack(int,void*)
{
	calcHist(&hue,1,&channels,Mat(),hue_hist,1,&h_bin,ranges,true,false);
	normalize(hue_hist,hue_hist,0,255,NORM_MINMAX);
	Mat backPrjImage;
	calcBackProject(&hue,1,&channels,hue_hist,backPrjImage,ranges,1);
	imshow("backPrjImage",backPrjImage);
}

