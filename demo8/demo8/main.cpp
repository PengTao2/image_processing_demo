#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src;

void MyLine(void);
void MyRectangle(void);
void MyEllipse(void);
void MyCircle(void);
void MyPolygon(void);

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/4.jpg");
	if(src.empty())
	{
		cout<<"Coulg not load image"<<endl;
		return -1;
	}

	MyLine();
	MyRectangle();
	MyEllipse();
	MyCircle();
	MyPolygon();
	putText(src,"This is a string...",Point(0,30),CV_FONT_HERSHEY_COMPLEX,1.0,Scalar(125,100,23),2,8);
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	waitKey(0);
	return 0;
}
void MyLine(void)
{
	Point p1 = Point(20,30);
	Point p2 = Point(300,300);
	Scalar color = Scalar(0,0,255);
	line(src,p1,p2,color,1,LINE_AA);
}
void MyRectangle(void)
{
	Rect rect = Rect(200,100,100,300);
	Scalar color = Scalar(0,0,255);
	rectangle(src,rect,color,2,LINE_8);
}
void MyEllipse(void)
{
	Scalar color = Scalar(0,0,255);
	ellipse(src,Point(src.cols / 2,src.rows /2),Point(src.cols / 8,src.rows /4),45,0,360,color,2,LINE_8);
}
void MyCircle(void)
{
	Scalar color = Scalar(0,0,255);
	Point center = Point(src.cols / 2,src.rows /2);
	circle(src,center,100,color,2,LINE_AA);
}
void MyPolygon(void)
{
	Scalar color = Scalar(0,0,255);
	int npt = 5;
	Point pts[1][5];
	pts[0][0] = Point(100,100);
	pts[0][1] = Point(100,200);
	pts[0][2] = Point(200,200);
	pts[0][3] = Point(200,100);
	pts[0][4] = Point(100,100);
	const Point* ppts[] = {pts[0]};
	fillPoly(src,ppts,&npt,1,color,8);
}