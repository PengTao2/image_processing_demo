#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc,char** argv)
{
	Mat src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);


	Mat dst;
	dst.create(src.size(),src.type());
	//blur(src,dst,Size(5,5));
	GaussianBlur(src,dst,Size(3,3),3,3);

	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	waitKey(0);
	return 0;
}

