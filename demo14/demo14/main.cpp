#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

Mat src,dst;

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	//pyrUp(src,dst,Size(src.cols * 2,src.rows * 2));
	pyrDown(src,dst,Size(src.cols / 2,src.rows / 2));
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	//DOG
	Mat gray_g,g1,g2,dog_image;
	cvtColor(src,gray_g,CV_RGB2GRAY);
	GaussianBlur(gray_g,g1,Size(5,5),0,0);
	GaussianBlur(g1,g2,Size(5,5),0,0);
	subtract(g1,g2,dog_image);
	normalize(dog_image,dog_image,255,0,NORM_MINMAX);
	imshow("DOG",dog_image);


	waitKey(0);
	return 0;
}
