#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

Mat src,dst;
int element_size = 3;
void CallBack(int,void*);

int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/4.jpg");
	if(src.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	createTrackbar("Element:","dst",&element_size,50,CallBack);
	CallBack(0,0);
	waitKey(0);
	return 0;
}

void CallBack(int,void*)
{
	int s = element_size * 2 + 1;
	Mat structele = getStructuringElement(MORPH_RECT,Size(s,s),Point(-1,-1));
	//dilate(src,dst,structele,Point(-1,-1),1);
	erode(src,dst,structele);
	imshow("dst",dst);
	return;
}

