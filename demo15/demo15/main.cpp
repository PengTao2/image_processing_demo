#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,gray,dst;
int m_thresdhold = 127;
int m_flag = 1;
int m_thresdhold_max = 255;
void ThresdHoldCallBack(int,void*);
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	cvtColor(src,gray,CV_BGR2GRAY);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);

	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	createTrackbar("Thresdhold:","dst",&m_thresdhold,255,ThresdHoldCallBack);
	createTrackbar("flag:","dst",&m_flag,5,ThresdHoldCallBack);
	ThresdHoldCallBack(0,0);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}

void ThresdHoldCallBack(int,void*)
{
	//threshold(gray,dst,m_thresdhold,m_thresdhold_max,CV_THRESH_BINARY);
	threshold(gray,dst,m_thresdhold,m_thresdhold_max,m_flag);
	imshow("dst",dst);
}