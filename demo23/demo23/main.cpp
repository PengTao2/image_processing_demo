#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,gray;
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);
	//建立映射表
	Mat map_x,map_y;
	map_x.create(src.size(),CV_32FC1);
	map_y.create(src.size(),CV_32FC1);
	for (int i = 0 ;i < src.rows;i +=2)
	{
		for(int j = 0;j < src.cols;j+=2)
		{
			map_x.at<float>(i,j) = j;//src.cols - j - 1;//列方向镜像
			map_y.at<float>(i,j) = i;//src.rows - i - 1;//行方向镜像
		}
	}
	remap(src,dst,map_x,map_y,CV_INTER_LINEAR,BORDER_CONSTANT);
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);
	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}
