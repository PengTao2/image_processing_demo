#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat src,dst,gray;
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not read Image.."<<endl;
		return -1;
	}
	namedWindow("src",CV_WINDOW_AUTOSIZE);
	imshow("src",src);

	cvtColor(src,gray,CV_BGR2GRAY);
	namedWindow("gray",CV_WINDOW_AUTOSIZE);
	imshow("gray",gray);

	equalizeHist(gray,dst);
	namedWindow("dst",CV_WINDOW_AUTOSIZE);
	imshow("dst",dst);

	cout<<"Press any key to exit.."<<endl;
	waitKey(0);
	return 0;
}
