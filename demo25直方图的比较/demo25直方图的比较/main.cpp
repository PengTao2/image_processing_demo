#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

string double2String(double d);

Mat src,src1,src2;
int main(int argc,char** argv)
{
	src = imread("D:/OpenCVTest/Images/5.jpg");
	if(src.empty())
	{
		cout<<"Could not load image"<<endl;
		return -1;
	}
	src1 = imread("D:/OpenCVTest/Images/5_0.jpg");
	src2 = imread("D:/OpenCVTest/Images/4.jpg");
	namedWindow("src");
	namedWindow("src1");
	namedWindow("src2");


	cvtColor(src,src,CV_RGB2HSV);
	cvtColor(src1,src1,CV_RGB2HSV);
	cvtColor(src2,src2,CV_RGB2HSV);

	//申明直方图H通道等级数，与S通道等级数
	int h_bin = 50,s_bin = 60;
	int hist_size[] = {h_bin,s_bin};
	//申明直方图H通道阈值范围，与S通道阈值范围
	float h_range[] = {0,180};
	float s_range[] = {0,255};
	const float* ranges[] = {h_range,s_range};
	//申明图像通道数H,S两个通道
	int channels[] = {0,1};
	//申明直方图输出结果因为是计算多通道图像的直方图结果所以用MatND申明
	MatND src_hist,src1_hist,src2_hist;
	calcHist(&src,1,channels,Mat(),src_hist,2,hist_size,ranges,true,false);
	normalize(src_hist,src_hist,0,1,NORM_MINMAX);

	calcHist(&src1,1,channels,Mat(),src1_hist,2,hist_size,ranges,true,false);
	normalize(src1_hist,src1_hist,0,1,NORM_MINMAX);

	calcHist(&src2,1,channels,Mat(),src2_hist,2,hist_size,ranges,true,false);
	normalize(src2_hist,src2_hist,0,1,NORM_MINMAX);

	double dst = compareHist(src_hist,src_hist,CV_COMP_CHISQR);
	double dst1 = compareHist(src_hist,src1_hist,CV_COMP_CORREL);
	double dst2 = compareHist(src_hist,src2_hist,CV_COMP_CORREL);
	double dst3 = compareHist(src1_hist,src2_hist,CV_COMP_CORREL);

	putText(src,double2String(dst),Point(50,50),1,CV_FONT_HERSHEY_COMPLEX,Scalar(0,255,255));
	putText(src1,double2String(dst1),Point(50,50),1,CV_FONT_HERSHEY_COMPLEX,Scalar(0,255,255));
	putText(src2,double2String(dst2),Point(50,50),1,CV_FONT_HERSHEY_COMPLEX,Scalar(0,255,255));

	imshow("src",src);
	imshow("src1",src1);
	imshow("src2",src2);

	waitKey(0);
	return 0;
}

string double2String(double d)
{
	ostringstream str;
	if(str << d)
	{
		return str.str();
	}
	return "invalid conversion";
}

