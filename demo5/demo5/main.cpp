#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc,char** argv)
{
	Mat src1 = imread("D:/OpenCVTest/Images/3.jpg");
	Mat src2 = imread("D:/OpenCVTest/Images/2.jpg");
	if(src1.empty() || src2.empty())
	{
		cout<<"Cound not load Image.."<<endl;
		return -1;
	}
	Mat dst;
	dst.create(src1.size(),src1.type());

	namedWindow("src1",CV_WINDOW_AUTOSIZE);
	namedWindow("src2",CV_WINDOW_AUTOSIZE);
	namedWindow("dst",CV_WINDOW_AUTOSIZE);

	addWeighted(src1,0.8,src2,0.2,0,dst);
	//add(src1,src2,dst);
	//multiply(src1,src2,dst);
	imshow("src1",src1);
	imshow("src2",src2);
	imshow("dst",dst);
	waitKey(0);
	return 0;
}

